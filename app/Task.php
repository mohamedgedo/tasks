<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'image',
        'name',
        'description',
    ];

    /**
     * upload image and save name in storage .
     *
     * @param $value
     *
     * @return mixed
     */
    public function setImageAttribute($value)
    {
        $image = $value->store('task');

        return $this->attributes['image'] = $image;
    }

    /**
     * return images belongs to task .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(Image::class);
    }

}
