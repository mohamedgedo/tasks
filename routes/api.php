<?php

use App\Task;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});

route::get('test', function (){
    //return response()->json(Task::all());
});

Route::get('task', 'TasksController@index');
Route::get('task/{id}', 'TasksController@show');
Route::post('task', 'TasksController@store');
Route::put('task/{id}', 'TasksController@update');
Route::delete('task/{id}', 'TasksController@delete');
